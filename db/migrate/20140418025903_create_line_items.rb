class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.float :quantity
      t.integer :beer_id
      t.integer :sales_order
      t.integer :tenant_id

      t.timestamps
    end
    add_index :line_items, :tenant_id
  end
end
