class AddUnableToSalesLineItems < ActiveRecord::Migration
  def change
    add_column :sales_line_items, :unable_to_sell, :float, default: 0.0
  end
end
