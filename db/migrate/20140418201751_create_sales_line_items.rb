class CreateSalesLineItems < ActiveRecord::Migration
  def change
    create_table :sales_line_items do |t|
      t.float :quantity
      t.integer :beer_id
      t.integer :sales_order
      t.integer :tenant_id

      t.timestamps
    end
    add_index :sales_line_items, :tenant_id
  end
end
