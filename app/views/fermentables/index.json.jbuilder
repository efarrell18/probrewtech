json.array!(@fermentables) do |fermentable|
  json.extract! fermentable, :id, :name, :type_of, :yield, :color, :notes, :price, :tenant
  json.url fermentable_url(fermentable, format: :json)
end
