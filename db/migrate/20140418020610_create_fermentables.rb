class CreateFermentables < ActiveRecord::Migration
  def change
    create_table :fermentables do |t|
      t.string :name
      t.string :type_of
      t.float :yield
      t.float :color
      t.text :notes
      t.decimal :price
      t.belongs_to :tenant

      t.timestamps
    end
  end
end
