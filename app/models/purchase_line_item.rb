class PurchaseLineItem < ActiveRecord::Base
    acts_as_tenant
  belongs_to :purchase_order
  belongs_to :material
  accepts_nested_attributes_for :material
  after_create :update_inventory_for_material

  private
        def update_inventory_for_material
            material = Material.find(self.material_id)
            converted_quantity = convert_measurement(material, self)
            quantity = material.quantity + converted_quantity
            material.update_attributes(quantity: quantity)
        end
        def convert_measurement(m, order)
            quantity = order.quantity
            if order.measurement == "Pounds"
                if m.measurement == "Kilograms"
                    quantity = order.quantity / 2.2046
                elsif m.measurement == "Ounces"
                    quantity = order.quantity * 16
                elsif m.measurement == "Grams"
                    quantity = order.quantity / 0.0022046
                end
            elsif order.measurement == "Kilograms"
                logger.info "Inside first"
                if m.measurement == "Pounds"
                    logger.info "inside second"
                    quantity = order.quantity * 2.2046
                elsif m.measurement == "Grams"
                    quantity = order.quantity * 1000
                elsif m.measurement == "Ounces"
                    quantity = order.quantity * 35.274
                end
            elsif order.measurement == "Ounces"
                if m.measurement == "Pounds"
                    quantity = order.quantity / 16
                elsif m.measurement == "Grams"
                    quantity = order.quantity / 0.035274
                elsif m.measurement == "Kilograms"
                    quantity = order.quantity / 35.274
                end
            elsif order.measurement == "Grams"
                if m.measurement == "Ounces"
                    quantity = order.quantity * 0.035274
                elsif m.measurement == "Kilograms"
                    quantity = order.quantity / 1000
                elsif m.measurement == "Pounds"
                    quantity = order.quantity * 0.0022046
                end
            end
            quantity
        end
end
