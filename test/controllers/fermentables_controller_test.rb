require 'test_helper'

class FermentablesControllerTest < ActionController::TestCase
  setup do
    @fermentable = fermentables(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fermentables)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fermentable" do
    assert_difference('Fermentable.count') do
      post :create, fermentable: { color: @fermentable.color, name: @fermentable.name, notes: @fermentable.notes, price: @fermentable.price, tenant: @fermentable.tenant, type_of: @fermentable.type_of, yield: @fermentable.yield }
    end

    assert_redirected_to fermentable_path(assigns(:fermentable))
  end

  test "should show fermentable" do
    get :show, id: @fermentable
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fermentable
    assert_response :success
  end

  test "should update fermentable" do
    patch :update, id: @fermentable, fermentable: { color: @fermentable.color, name: @fermentable.name, notes: @fermentable.notes, price: @fermentable.price, tenant: @fermentable.tenant, type_of: @fermentable.type_of, yield: @fermentable.yield }
    assert_redirected_to fermentable_path(assigns(:fermentable))
  end

  test "should destroy fermentable" do
    assert_difference('Fermentable.count', -1) do
      delete :destroy, id: @fermentable
    end

    assert_redirected_to fermentables_path
  end
end
