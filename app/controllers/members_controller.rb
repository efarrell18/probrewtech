class MembersController < ApplicationController
    def index
        @members = Member.all
    end
    def new
        @member = Member.new
        @user   = User.new
    end
    def edit
        @member = Member.find(params[:id])
    end
    def update
        @member = Member.find(params[:id])
        if @member.update_attributes(member_params)
            flash[:success] = "Updated Successfully"
            render :edit
        else
            flash[:error] = "Problem updating"
            render :edit
        end
    end

    def create
        @user   = User.new(user_params)

        # ok to create user, member
        if @user.save_and_invite_member && @user.create_member(member_params)
            flash[:notice] = "New member added and invitation email sent to #{@user.email}."
            redirect_to root_path
        else
            flash[:error] = "errors occurred!"
            @member = Member.new(member_params) # only used if need to revisit form
            render :new
        end

    end


    private

        def member_params
            params.require(:member).permit(:first_name, :last_name, :position_id)
        end

        def user_params
            params.require(:user).permit(:email, :password, :password_confirmation)
        end
end
