class CreateDemands < ActiveRecord::Migration
  def change
    create_table :demands do |t|
      t.decimal :demand
      t.integer :tenant_id

      t.timestamps
    end
    add_index :demands, :tenant_id
  end
end
