class CreateYeasts < ActiveRecord::Migration
  def change
    create_table :yeasts do |t|
      t.string :name
      t.string :product
      t.string :laboratory
      t.string :form
      t.float :attenuation
      t.string :type_of
      t.text :notes
      t.decimal :price
      t.integer :tenant_id

      t.timestamps
    end
  end
end
