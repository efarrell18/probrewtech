class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.float :quantity
      t.string :measurement
      t.integer :materiable_id
      t.string :materiable_type
      t.integer :tenant_id

      t.timestamps
    end
    add_index :inventories, :tenant_id
  end
end
