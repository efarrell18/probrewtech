json.array!(@hops) do |hop|
  json.extract! hop, :id, :beta, :alpha, :name, :form, :origin, :usage, :price, :tenant_id
  json.url hop_url(hop, format: :json)
end
