# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery ->
    Morris.Line
        element: 'revenues_chart'
        data: $('#weekly_revenue').data('revenue')
        xkey: 'week_start'
        ykeys: ['revenue', 'expenses']
        labels: ['Revenue', 'Expenses']
