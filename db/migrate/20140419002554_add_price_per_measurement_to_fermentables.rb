class AddPricePerMeasurementToFermentables < ActiveRecord::Migration
  def change
    add_column :fermentables, :price_per_unit, :string
  end
end
