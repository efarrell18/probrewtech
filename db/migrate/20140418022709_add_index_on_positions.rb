class AddIndexOnPositions < ActiveRecord::Migration
  def change
      add_index :positions, :tenant_id
  end
end
