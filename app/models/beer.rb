class Beer < ActiveRecord::Base
    acts_as_tenant
  belongs_to :recipe
  around_create :brew_beer

  def Beer.weekly_cost
      cost_per_week = 0.0
      count = 0
      first_day = Beer.first.created_at
      start_date = Date.today
      first_day = first_day > start_date - 1.month ? first_day : start_date - 1.month
      end_date = start_date - 1.week
      while end_date >= first_day
          Beer.where("created_at <= :start_date AND created_at > :end_date", start_date: start_date, end_date: end_date).each do |b|
              cost = 0
              b.recipe.recipe_fermentables.each do |f|
                  cost += f.quantity * f.fermentable.price
              end
              b.recipe.recipe_yeasts.each do |y|
                  cost += y.quantity * y.yeast.price
              end
              b.recipe.recipe_hops.each do |h|
                  cost += h.quantity * h.hop.price
              end
              cost_per_week += cost
          end
          count += 1
          start_date = end_date
          end_date = start_date - 1.week
      end
      cost_per_week / count
  end
  private
    def brew_beer
        set_max_size
        remove_from_inventory
        yield
        add_to_finished_goods
    end
    def set_max_size
        max_size = 1.0
        self.recipe.recipe_fermentables.each do |f|
            material = Material.find(f.fermentable.material.id)
            size = material.quantity > f.quantity ? 1.0 : (material.quantity / f.quantity)
            max_size = max_size <= size ? max_size : size
        end
        self.recipe.recipe_yeasts.each do |y|
            material = Material.find( y.yeast.material.id)
            size = material.quantity > y.quantity ? 1.0 : (material.quantity / y.quantity)
            max_size = max_size <= size ? max_size : size
        end
        self.recipe.recipe_hops.each do |h|
            material = Material.find(h.hop.material.id)
            size = material.quantity > h.quantity ? 1.0 : (material.quantity / h.quantity)
            max_size = max_size <= size ? max_size : size
        end
        self.quantity = max_size
    end

    def remove_from_inventory
        self.recipe.recipe_fermentables.each do |f|
            material = Material.find(f.fermentable.material.id)
            quantity = material.quantity - (f.quantity * self.quantity)
            material.update_attributes(quantity: quantity)
        end
        self.recipe.recipe_yeasts.each do |y|
            material = Material.find( y.yeast.material.id)
            quantity = material.quantity - (y.quantity * self.quantity)
            material.update_attributes(quantity: quantity)
        end
        self.recipe.recipe_hops.each do |h|
            material = Material.find(h.hop.material.id)
            quantity = material.quantity - (h.quantity * self.quantity)
            material.update_attributes(quantity: quantity)
        end
    end
    def add_to_finished_goods
        finished_good = FinishedGood.find(self.recipe.finished_good.id)
        quantity = finished_good.quantity + (self.recipe.size * self.quantity)
        finished_good.update_attributes(quantity: quantity)
    end
end
