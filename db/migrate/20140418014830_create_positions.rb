class CreatePositions < ActiveRecord::Migration
  def change
    create_table :positions do |t|
      t.string :name
      t.string :pay_type
      t.decimal :pay_rate
      t.text :description

      t.timestamps
    end
  end
end
