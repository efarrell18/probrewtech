class CreatePurchaseOrders < ActiveRecord::Migration
  def change
    create_table :purchase_orders do |t|
      t.integer :vendor_id
      t.integer :tenant_id

      t.timestamps
    end
    add_index :purchase_orders, :vendor_id
    add_index :purchase_orders, :tenant_id
  end
end
