class AddMeasuremenToPurchaseLineItems < ActiveRecord::Migration
  def change
    add_column :purchase_line_items, :measurement, :string
  end
end
