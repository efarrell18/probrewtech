class RemovePriceFromBeer < ActiveRecord::Migration
  def up
      remove_column :beers, :price
  end
  def down
      add_column :beers, :price, :decimal
  end
end
