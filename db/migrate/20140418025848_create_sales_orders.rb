class CreateSalesOrders < ActiveRecord::Migration
  def change
    create_table :sales_orders do |t|
      t.integer :customer_id
      t.integer :tenant_id

      t.timestamps
    end
    add_index :sales_orders, :tenant_id
  end
end
