json.array!(@positions) do |position|
  json.extract! position, :id, :name, :pay_type, :pay_rate, :description
  json.url position_url(position, format: :json)
end
