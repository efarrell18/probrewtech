Probrewtech::Application.routes.draw do
  resources :materials, only: [:index, :show]
  resources :finished_goods, only: :index
  resources :revenues, only: :index
  resources :demands, only: :index

  resources :purchase_orders

  resources :sales_orders

  resources :beers

  resources :recipes

  resources :yeasts

  resources :fermentables

  resources :hops

  resources :positions

  resources :members
  resources :expenses, only: :index

  root to: 'static_pages#home'
  as :user do
      match '/user/confirmation' => 'milia/confirmations#update', via: :put, as: :update_user_confirmation
  end
  
  devise_for :users, :controllers => {
      registrations: 'milia/registrations',
      confirmations: 'milia/confirmations',
      sessions: 'milia/sessions',
      passwords: 'milia/passwords',
  }
  match '/dashboard', as: 'user_root_path', via: 'get', to: 'dashboard#index'
  get "dashboard/index"


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
