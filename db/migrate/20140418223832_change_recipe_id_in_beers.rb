class ChangeRecipeIdInBeers < ActiveRecord::Migration
  def up
      rename_column :beers, :recipe_id_id, :recipe_id
  end
  def down
      rename_column :beers, :recipe_id, :recipe_id_id
  end
end
