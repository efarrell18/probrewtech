class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :authenticate_tenant!
  before_action :prep_org_name
  rescue_from ::Milia::Control::MaxTenantExceeded, with: :max_tenants
  rescue_from ::Milia::Control::InvalidTenantAccess, with: :invalid_tenant

  private
    def callback_authenticate_tenant
        @org_name = Tenant.current_tenant.nil? ? "ProBrewTech" : Tenant.current_tenant.name
    end
    def prep_org_name
        @org_name ||= "ProBrewTech"
    end
end
