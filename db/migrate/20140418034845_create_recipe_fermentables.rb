class CreateRecipeFermentables < ActiveRecord::Migration
  def change
    create_table :recipe_fermentables do |t|
      t.float :quantity
      t.string :measurement
      t.integer :tenant_id
      t.references :recipe, index: true
      t.references :fermentable, index: true

      t.timestamps
    end
  end
end
