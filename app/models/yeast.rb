class Yeast < ActiveRecord::Base
    acts_as_tenant
    has_one :material, as: :materiable, dependent: :destroy
    has_many :purchase_line_items, as: :purchasable
end
