class SalesOrder < ActiveRecord::Base
    acts_as_tenant
    has_many :sales_line_items
    has_many :beers, through: :sales_line_items
    accepts_nested_attributes_for :sales_line_items

    def SalesOrder.weekly_revenue
      revenue_per_week = 0.0
      count = 0
      first_day = SalesOrder.first.created_at
      start_date = Date.today
      first_day = first_day > start_date - 1.month ? first_day : start_date - 1.month
      end_date = start_date - 1.week
      while end_date >= first_day
          SalesOrder.where("created_at <= :start_date AND created_at > :end_date", start_date: start_date, end_date: end_date).each do |b|
              b.sales_line_items.each do |s|
                  revenue_per_week += s.quantity * 45
              end
          end
          count += 1
          start_date = end_date
          end_date = start_date - 1.week
      end
      revenue_per_week / count
    end
    def SalesOrder.revenue_by_week
      revenue_per_week = Array.new
      first_day = SalesOrder.first.created_at
      start_date = Date.today.beginning_of_week
      first_day = first_day > start_date - 3.months ? first_day : start_date - 3.months
      end_date = start_date - 1.week
      while end_date >= first_day
          revenue_for_week = 0
          SalesOrder.where("created_at <= :start_date AND created_at > :end_date", start_date: start_date, end_date: end_date).each do |b|
              b.sales_line_items.each do |s|
                  revenue_for_week += s.quantity * 45
              end
          end
          revenue_per_week << {revenue: revenue_for_week, week_start: start_date}
          start_date = end_date
          end_date = start_date - 1.week
      end
      revenue_per_week
    end
    def SalesOrder.expenses_and_revenue_by_week
      revenue_per_week = Array.new
      first_day = SalesOrder.first.created_at
      start_date = Date.today.beginning_of_week
      first_day = first_day > start_date - 3.months ? first_day : start_date - 3.months
      end_date = start_date - 1.week
      while end_date >= first_day
          revenue_for_week = 0
          SalesOrder.where("created_at <= :start_date AND created_at > :end_date", start_date: start_date, end_date: end_date).each do |b|
              b.sales_line_items.each do |s|
                  revenue_for_week += s.quantity * 50
              end
          end
          expenses_per_week = 0
          Beer.where("created_at <= :start_date AND created_at > :end_date", start_date: start_date, end_date: end_date).each do |b|
              cost = 0
              b.recipe.recipe_fermentables.each do |f|
                  cost += f.quantity * f.fermentable.price
              end
              b.recipe.recipe_yeasts.each do |y|
                  cost += y.quantity * y.yeast.price
              end
              b.recipe.recipe_hops.each do |h|
                  cost += h.quantity * h.hop.price
              end
              expenses_per_week = (b.quantity * cost) + Member.weekly_labor_cost
          end
          revenue_per_week << {revenue: revenue_for_week, week_start: start_date, expenses: expenses_per_week}
          start_date = end_date
          end_date = start_date - 1.week
      end
      revenue_per_week
    end
    private
end
