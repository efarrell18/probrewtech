class CreatePurchaseLineItems < ActiveRecord::Migration
  def change
    create_table :purchase_line_items do |t|
      t.float :quantity
      t.integer :material_id
      t.integer :tenant_id
      t.integer :purchase_order_id

      t.timestamps
    end
    add_index :purchase_line_items, :tenant_id
    add_index :purchase_line_items, [:purchase_order_id, :material_id]
  end
end
