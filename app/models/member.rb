class Member < ActiveRecord::Base
  belongs_to :user
  belongs_to :position
    acts_as_tenant
    
    DEFAULT_ADMIN = {
        first_name: "Admin",
        last_name: "Please edit me"
    }

    def self.create_new_member(user, params)

        return user.create_member(params)
    end
    def self.create_org_admin(user)
        new_member = create_new_member(user, DEFAULT_ADMIN)
        unless new_member.errors.empty?
            raise ArgumentError, new_member.errors.full_messages.uniq.join(", ")
        end

        return new_member
    end
    def self.weekly_labor_cost
        cost = 0.0
        Member.all.each do |m|
            unless m.position.nil?
                if m.position.pay_type == 'Salary'
                    cost = m.position.pay_rate / 52.0
                elsif m.position.pay_type == 'Hourly'
                    cost = m.position.pay_rate * 40
                end
            end
        end
        cost
    end
end
