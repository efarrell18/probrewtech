class AddPricePerMeasurementToYeasts < ActiveRecord::Migration
  def change
    add_column :yeasts, :price_per_unit, :string
  end
end
