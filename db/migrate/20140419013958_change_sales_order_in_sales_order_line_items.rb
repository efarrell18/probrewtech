class ChangeSalesOrderInSalesOrderLineItems < ActiveRecord::Migration
  def up
      rename_column :sales_line_items, :sales_order, :sales_order_id
  end
  def down
      rename_column :sales_line_items, :sales_order_id, :sales_order
  end
end
