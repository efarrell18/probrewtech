class Recipe < ActiveRecord::Base
    acts_as_tenant
    SOLID_MEASUREMENTS = ["Pounds", "Kilograms", "Ounces", "Grams"]
    LIQUID_MEASUREMENTS = ["Liters", "Gallons", "Quarts"]
    #fermentables
    has_many :recipe_fermentables, dependent: :destroy
    has_many :fermentables, through: :recipe_fermentables
    accepts_nested_attributes_for :recipe_fermentables
    #hops
    has_many :recipe_hops
    has_many :hops, through: :recipe_hops, dependent: :destroy
    accepts_nested_attributes_for :recipe_hops
    #yeasts has_many :recipe_yeasts
    has_many :recipe_yeasts, dependent: :destroy
    has_many :yeasts, through: :recipe_yeasts
    accepts_nested_attributes_for :recipe_yeasts

    has_many :beers
    has_one :finished_good

    def total_cost
        cost = 0.0
        self.recipe_fermentables.each do |f|
            cost += f.quantity * f.fermentable.price
        end
        self.recipe_yeasts.each do |y|
            cost += y.quantity * y.yeast.price
        end
        self.recipe_hops.each do |h|
            cost += h.quantity * h.hop.price
        end
        cost
    end
    def cost_per_bottle
        total_cost / self.size / 10.666
    end
    def cost_per_keg
        total_cost / self.size * 15.5
    end

end
