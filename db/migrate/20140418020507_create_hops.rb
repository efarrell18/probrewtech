class CreateHops < ActiveRecord::Migration
  def change
    create_table :hops do |t|
      t.float :beta
      t.float :alpha
      t.string :name
      t.string :form
      t.string :origin
      t.string :usage
      t.decimal :price
      t.belongs_to :tenant, index: true

      t.timestamps
    end
  end
end
