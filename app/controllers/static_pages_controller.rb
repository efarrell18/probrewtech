class StaticPagesController < ApplicationController
    skip_before_action :authenticate_tenant!, only: :home
  # this will give you improved handling for letting user know
  # # what is expected. If you want to have a welcome page for
  # # signed in users, uncomment the redirect_to line, etc.
  def home
      if user_signed_in?
          flash[:notice] = flash[:error] unless flash[:error].blank?
          # redirect_to welcome_path
      else

          if flash[:notice].blank?
              flash[:notice] = "sign in if your organization has an account"
          end
      end
      
  end
end
