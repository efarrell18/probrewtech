# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140419055529) do

  create_table "beers", force: true do |t|
    t.integer  "recipe_id"
    t.decimal  "quantity"
    t.integer  "tenant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "beers", ["recipe_id"], name: "index_beers_on_recipe_id"

  create_table "demands", force: true do |t|
    t.decimal  "demand"
    t.integer  "tenant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "demands", ["tenant_id"], name: "index_demands_on_tenant_id"

  create_table "fermentables", force: true do |t|
    t.string   "name"
    t.string   "type_of"
    t.float    "yield"
    t.float    "color"
    t.text     "notes"
    t.decimal  "price"
    t.integer  "tenant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "price_per_unit"
  end

  add_index "fermentables", ["tenant_id"], name: "index_fermentables_on_tenant_id"

  create_table "finished_goods", force: true do |t|
    t.float    "quantity"
    t.string   "measurement"
    t.integer  "recipe_id"
    t.integer  "tenant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "finished_goods", ["recipe_id"], name: "index_finished_goods_on_recipe_id"

  create_table "hops", force: true do |t|
    t.float    "beta"
    t.float    "alpha"
    t.string   "name"
    t.string   "form"
    t.string   "origin"
    t.string   "usage"
    t.decimal  "price"
    t.integer  "tenant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "price_per_unit"
  end

  add_index "hops", ["tenant_id"], name: "index_hops_on_tenant_id"

  create_table "materials", force: true do |t|
    t.float    "quantity"
    t.string   "measurement"
    t.integer  "materiable_id"
    t.string   "materiable_type"
    t.integer  "tenant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "materials", ["tenant_id"], name: "index_materials_on_tenant_id"

  create_table "members", force: true do |t|
    t.integer  "tenant_id"
    t.integer  "user_id"
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "position_id"
  end

  add_index "members", ["tenant_id"], name: "index_members_on_tenant_id"
  add_index "members", ["user_id"], name: "index_members_on_user_id"

  create_table "positions", force: true do |t|
    t.string   "name"
    t.string   "pay_type"
    t.decimal  "pay_rate"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "tenant_id"
  end

  add_index "positions", ["tenant_id"], name: "index_positions_on_tenant_id"

  create_table "purchase_line_items", force: true do |t|
    t.float    "quantity"
    t.integer  "material_id"
    t.integer  "tenant_id"
    t.integer  "purchase_order_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "measurement"
  end

  add_index "purchase_line_items", ["purchase_order_id", "material_id"], name: "index_purchase_line_items_on_purchase_order_id_and_material_id"
  add_index "purchase_line_items", ["tenant_id"], name: "index_purchase_line_items_on_tenant_id"

  create_table "purchase_orders", force: true do |t|
    t.integer  "vendor_id"
    t.integer  "tenant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "purchase_orders", ["tenant_id"], name: "index_purchase_orders_on_tenant_id"
  add_index "purchase_orders", ["vendor_id"], name: "index_purchase_orders_on_vendor_id"

  create_table "recipe_fermentables", force: true do |t|
    t.float    "quantity"
    t.string   "measurement"
    t.integer  "tenant_id"
    t.integer  "recipe_id"
    t.integer  "fermentable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "recipe_fermentables", ["fermentable_id"], name: "index_recipe_fermentables_on_fermentable_id"
  add_index "recipe_fermentables", ["recipe_id"], name: "index_recipe_fermentables_on_recipe_id"

  create_table "recipe_hops", force: true do |t|
    t.float    "quantity"
    t.string   "measurement"
    t.integer  "tenant_id"
    t.integer  "recipe_id"
    t.integer  "hop_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "recipe_yeasts", force: true do |t|
    t.float    "quantity"
    t.string   "measurement"
    t.integer  "tenant_id"
    t.integer  "recipe_id"
    t.integer  "yeast_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "recipe_yeasts", ["tenant_id"], name: "index_recipe_yeasts_on_tenant_id"

  create_table "recipes", force: true do |t|
    t.integer  "size"
    t.text     "description"
    t.integer  "tenant_id"
    t.integer  "fermentation_time"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  add_index "recipes", ["tenant_id"], name: "index_recipes_on_tenant_id"

  create_table "sales_line_items", force: true do |t|
    t.float    "quantity"
    t.integer  "beer_id"
    t.integer  "sales_order_id"
    t.integer  "tenant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "measurement"
    t.float    "unable_to_sell", default: 0.0
  end

  add_index "sales_line_items", ["tenant_id"], name: "index_sales_line_items_on_tenant_id"

  create_table "sales_orders", force: true do |t|
    t.integer  "customer_id"
    t.integer  "tenant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sales_orders", ["tenant_id"], name: "index_sales_orders_on_tenant_id"

  create_table "sessions", force: true do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at"

  create_table "tenants", force: true do |t|
    t.integer  "tenant_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tenants", ["name"], name: "index_tenants_on_name"
  add_index "tenants", ["tenant_id"], name: "index_tenants_on_tenant_id"

  create_table "tenants_users", id: false, force: true do |t|
    t.integer "tenant_id", null: false
    t.integer "user_id",   null: false
  end

  add_index "tenants_users", ["tenant_id", "user_id"], name: "index_tenants_users_on_tenant_id_and_user_id"

  create_table "users", force: true do |t|
    t.string   "email",                        default: "",   null: false
    t.string   "encrypted_password",           default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.boolean  "skip_confirm_change_password", default: true
    t.integer  "tenant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "yeasts", force: true do |t|
    t.string   "name"
    t.string   "product"
    t.string   "laboratory"
    t.string   "form"
    t.float    "attenuation"
    t.string   "type_of"
    t.text     "notes"
    t.decimal  "price"
    t.integer  "tenant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "price_per_unit"
  end

  add_index "yeasts", ["tenant_id"], name: "index_yeasts_on_tenant_id"

end
