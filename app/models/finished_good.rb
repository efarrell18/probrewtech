class FinishedGood < ActiveRecord::Base
    acts_as_tenant
    belongs_to :recipe
end
