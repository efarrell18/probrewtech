class RecipeYeast < ActiveRecord::Base
  belongs_to :recipe
  belongs_to :yeast
  accepts_nested_attributes_for :yeast
end
