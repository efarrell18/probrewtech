class PurchaseOrder < ActiveRecord::Base
    acts_as_tenant
    has_many :purchase_line_items
    has_many :materials, through: :purchase_line_items
    accepts_nested_attributes_for :purchase_line_items

end
