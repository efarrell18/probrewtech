class CreateBeers < ActiveRecord::Migration
  def change
    create_table :beers do |t|
      t.belongs_to :recipe_id, index: true
      t.decimal :quantity
      t.decimal :price
      t.integer :tenant_id

      t.timestamps
    end
  end
end
