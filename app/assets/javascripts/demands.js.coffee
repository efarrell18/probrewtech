# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery ->
    Morris.Line
        element: 'demand_chart'
        data: $('#weekly_demand').data('demand')
        xkey: 'week_start'
        ykeys: ['demand', 'forecast', 'missed' ]
        labels: ['Kegs', 'Forecast', 'Missed Sales']
