json.array!(@inventories) do |inventory|
  json.extract! inventory, :id, :quantity, :measurement, :materiable_id, :materiable_type, :tenant_id
  json.url inventory_url(inventory, format: :json)
end
