json.array!(@recipes) do |recipe|
  json.extract! recipe, :id, :size, :description, :tenant_id, :fermentation_time
  json.url recipe_url(recipe, format: :json)
end
