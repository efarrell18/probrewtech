class AddTenantToPositions < ActiveRecord::Migration
  def change
    add_column :positions, :tenant_id, :integer
  end
end
