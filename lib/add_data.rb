module AddData
    def self.add_data(user, tenant)
        reset_test_data(user, tenant)
        @recipe = Recipe.first
        @hop_material = @recipe.recipe_hops.first.hop.material
        @fermentable_material = @recipe.recipe_fermentables.first.fermentable.material
        @yeast_material = @recipe.recipe_yeasts.first.yeast.material
        @count = 0
        @random_sample = [0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0, 1.05, 1.10, 1.15, 1.20, 1.25, 1.30, 1.35, 1.40, 1.45, 1.50]
        @last_sample = @random_sample.sample
        @safety_stock = 186.0

        ActiveRecord::Base.transaction  do
            start = Date.today - 5.months
            start = start.beginning_of_week
            @start_day = start.wday
            work_days = [1, 2, 3, 4, 5]
            ((start)..(Date.today)).each do |date|
                @left_over_quantity =  FinishedGood.find(@recipe.finished_good.id).quantity
                @purchase_factor = ((Demand.demand_for_week * 15.5) - (@left_over_quantity)) / @recipe.size / 5
                if date.wday == 1 
                    add_purchase_order(date)
                end
                if @left_over_quantity < @safety_stock
                    smaller_purchase_order(date)
                    brew_more(date)
                end
                if work_days.include?(date.wday)
                    add_brew(date)
                    add_sales_order(date)
                end
            end
        end
    end
    def self.add_purchase_order(date)
        if @count > 0
            Demand.create(created_at: date, demand: Demand.demand_for_week(date))
        else
            Demand.create(created_at: date, demand: 150.0)
        end
        
        f_quan = @fermentable_material.quantity 
        y_quan = @yeast_material.quantity 
        h_quan = @hop_material.quantity 
        PurchaseOrder.create(created_at: date, purchase_line_items_attributes: [quantity: ((@purchase_factor * 5670) - f_quan), measurement: "Pounds", material_id: @fermentable_material.id])
        PurchaseOrder.create(created_at: date, purchase_line_items_attributes: [quantity: ((@purchase_factor * 5) - y_quan), measurement: "Gallons", material_id: @yeast_material.id])
        PurchaseOrder.create(created_at: date, purchase_line_items_attributes: [quantity: ((@purchase_factor * 30) - h_quan), measurement: "Pounds", material_id: @hop_material.id])
        @count += 1
    end
    def self.smaller_purchase_order(date)
        
        factor = 0.4
        PurchaseOrder.create(created_at: date, purchase_line_items_attributes: [quantity: (factor * 1134), measurement: "Pounds", material_id: @fermentable_material.id])
        PurchaseOrder.create(created_at: date, purchase_line_items_attributes: [quantity: (factor * 1), measurement: "Gallons", material_id: @yeast_material.id])
        PurchaseOrder.create(created_at: date, purchase_line_items_attributes: [quantity: (factor * 6), measurement: "Pounds", material_id: @hop_material.id])
    end
    def self.add_brew(date)
        @beer = Beer.create(recipe_id: @recipe.id, quantity: (@purchase_factor), created_at: date)
    end
    def self.brew_more(date)
        @beer = Beer.create(recipe_id: @recipe.id, quantity: (@safety_stock / @recipe.size), created_at: date)
    end

    def self.add_sales_order(date)
        quantity = (30 * calculate_new_sample).to_i
        SalesOrder.create(created_at: date, sales_line_items_attributes: [quantity: quantity, measurement: "Kegs", beer_id: @beer.id])
    end
    def self.change_tenant(my_id, my_tenant_id)
        @me = User.find(my_id)
        @w = Tenant.find(my_tenant_id)
        Tenant.set_current_tenant @w
    end
    def self.reset_test_data(user, tenant)
        change_tenant(user, tenant)
        Demand.delete_all
        PurchaseOrder.delete_all
        SalesOrder.delete_all
        Beer.delete_all
        Material.all.each { |m| m.update_attributes(quantity: 0)}
        FinishedGood.all.each { |m| m.update_attributes(quantity: 0)}
    end
    def self.calculate_new_sample
        new_sample = @random_sample.sample
        sample = new_sample + (0.3 * (@last_sample - new_sample))
        @last_sample = sample
        sample
    end


end
