class CreateMaterials < ActiveRecord::Migration
  def change
    create_table :materials do |t|
      t.float :quantity
      t.string :measurement
      t.integer :materiable_id
      t.string :materiable_type
      t.integer :tenant_id

      t.timestamps
    end
    add_index :materials, :tenant_id
  end
end
