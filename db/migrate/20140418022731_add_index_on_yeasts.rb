class AddIndexOnYeasts < ActiveRecord::Migration
  def change
      add_index :yeasts, :tenant_id
  end
end
