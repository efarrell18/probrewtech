class CreateFinishedGoods < ActiveRecord::Migration
  def change
    create_table :finished_goods do |t|
      t.float :quantity
      t.string :measurement
      t.integer :recipe_id
      t.integer :tenant_id

      t.timestamps
    end
    add_index :finished_goods, :recipe_id
  end
end
