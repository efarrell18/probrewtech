class AddPricePerMeasurementToHops < ActiveRecord::Migration
  def change
    add_column :hops, :price_per_unit, :string
  end
end
