class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.integer :size
      t.text :description
      t.integer :tenant_id
      t.integer :fermentation_time

      t.timestamps
    end
  end
end
