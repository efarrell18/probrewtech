class Demand < ActiveRecord::Base
    acts_as_tenant
    def Demand.demand_for_week(date = Date.today)
        alpha = 0.3
        last_forecast = Demand.last.present? ? Demand.last.demand : (30.0 * 5)
        puts last_forecast
        actual_demand = Demand.demand_for_last_week(date)
        puts actual_demand
        actual_demand = actual_demand == 0 ? 150 : actual_demand
        puts actual_demand
        demand = last_forecast + (alpha * (actual_demand - last_forecast))
        demand
    end

    def Demand.demand_for_last_week(date = Date.today)
        demand_last_week = 0.0
      SalesOrder.where("created_at <= :start_date AND created_at > :end_date", start_date: date.beginning_of_week, end_date: (date.beginning_of_week - 1.week)).each do |b|
          unless b.nil?
              b.sales_line_items.each do |s|
                  demand_last_week += s.quantity 
              end
          end
      end
      demand_last_week
    end
    def Demand.weekly_demand
        count = 0
      demand_for_week = Array.new
      first_day = SalesOrder.first.created_at
      start_date = Date.today.beginning_of_week
      first_day = first_day > start_date - 3.months ? first_day : start_date - 3.months
      end_date = start_date - 1.week
      total_missed = 0
      while end_date >= first_day
          demand_per_week = 0
          missed_per_week = 0
          SalesOrder.where("created_at <= :start_date AND created_at > :end_date", start_date: start_date, end_date: end_date).each do |b|
              b.sales_line_items.each do |s|
                  demand_per_week += s.quantity 
                  missed_per_week += s.unable_to_sell
              end
          end
          forecast = Demand.where("created_at <= :start_date AND created_at > :end_date", start_date: start_date, end_date: end_date).first.demand
          demand_for_week << {demand: demand_per_week, week_start: start_date, forecast: forecast, missed: missed_per_week}
          start_date = end_date
          end_date = start_date - 1.week
          total_missed += missed_per_week
          count += 1
      end
      logger.info "Missed"
      logger.info total_missed / count
      demand_for_week
    end

end
