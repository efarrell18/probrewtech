class CreateRecipeYeasts < ActiveRecord::Migration
  def change
    create_table :recipe_yeasts do |t|
      t.float :quantity
      t.string :measurement
      t.integer :tenant_id
      t.integer :recipe_id
      t.integer :yeast_id

      t.timestamps
    end
    add_index :recipe_yeasts, :tenant_id
  end
end
