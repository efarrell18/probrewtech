class LineItem < ActiveRecord::Base
    acts_as_tenant
    belongs_to :beer
    belongs_to :sales_order
end
