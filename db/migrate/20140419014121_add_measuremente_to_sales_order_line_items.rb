class AddMeasurementeToSalesOrderLineItems < ActiveRecord::Migration
  def change
    add_column :sales_line_items, :measurement, :string
  end
end
