class AddIndexOnFermentables < ActiveRecord::Migration
  def change
      add_index :fermentables, :tenant_id
  end
end
