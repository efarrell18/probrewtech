json.array!(@beers) do |beer|
  json.extract! beer, :id, :recipe_id_id, :quantity, :price, :tenant_id
  json.url beer_url(beer, format: :json)
end
