class SalesLineItem < ActiveRecord::Base
    acts_as_tenant
    belongs_to :sales_order
    belongs_to :beer
    accepts_nested_attributes_for :beer
    #before_create :set_max_order_size
    before_save :set_max_order_size
    after_create :update_finished_goods
    MEASUREMENT_TYPES = ["Kegs"]
  private
        def update_finished_goods
            finished_good = FinishedGood.find(self.beer.recipe.finished_good.id)
            converted_quantity = convert_measurement(finished_good, self)
            quantity = finished_good.quantity - converted_quantity
            finished_good.update_attributes(quantity: quantity)
        end
        def convert_measurement(b, order)
            quantity = order.quantity
            if order.measurement == "Kegs"
                quantity = quantity * 15.5
            elsif order.measurement == "Gallons"
                if b.measurement == "Quarts"
                    quantity = order.quantity * 4
                elsif b.measurement == "Liters"
                    quantity = order.quantity * 3.78541
                end
            elsif order.measurement == "Liters"
                if b.measurement == "Quarts"
                    logger.info "inside second"
                    quantity = order.quantity * 2.2046
                elsif b.measurement == "Gallons"
                    quantity = order.quantity * 1.05669
                end
            elsif order.measurement == "Quarts"
                if b.measurement == "Gallons"
                    quantity = order.quantity / 4
                elsif b.measurement == "Liters"
                    quantity = order.quantity / 3.78541
                end
            end
            quantity
        end
        def set_max_order_size
            finished_good = FinishedGood.find(beer.recipe.finished_good.id)
            if (self.quantity * 15.5) > finished_good.quantity
                new_quantity = (finished_good.quantity / 15.5).to_i
                self.unable_to_sell = self.quantity - new_quantity 
                self.quantity = new_quantity
            end
        end
end
