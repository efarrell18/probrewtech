class AddIndexOnRecipes < ActiveRecord::Migration
  def change
      add_index :recipes, :tenant_id
  end
end
