class Material < ActiveRecord::Base
    acts_as_tenant
    belongs_to :materiable, polymorphic: true
    has_many :purchase_line_items
end
