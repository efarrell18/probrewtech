class CreateRecipeHops < ActiveRecord::Migration
  def change
    create_table :recipe_hops do |t|
      t.float :quantity
      t.string :measurement
      t.integer :tenant_id
      t.integer :recipe_id
      t.integer :hop_id

      t.timestamps
    end
  end
end
