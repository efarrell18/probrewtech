class Position < ActiveRecord::Base
    acts_as_tenant
    has_many :members
    PAY_TYPES = ["Salary", "Hourly"]
    validates :pay_type, presence: true, inclusion: { in: PAY_TYPES }
end
