json.array!(@yeasts) do |yeast|
  json.extract! yeast, :id, :name, :product, :laboratory, :form, :attenuation, :type_of, :notes, :price, :tenant_id
  json.url yeast_url(yeast, format: :json)
end
